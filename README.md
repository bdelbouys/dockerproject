# dockerProject

Cloud Infrastructures at EISTI - Bastien Delbouys, M2 TI, UPPA - Oct 2019

The goal of the project was to dockerize an application of our choice.
The application I chose was a simple Graphical CRUD Java application working with a MYSQL database.

The project is composed of two majhor parts;
 - The 'app' folder, containing the source code, some external libraries needed for the application and a Dockerfile. 
 - The 'database' folder, containg the sql script needed to initialize the database.

The docker part of the project is working as followed :
 - By using the 'docker-compose up' command, 2 containers will be create.
    * One, containing a MYSQL database. This container is using a shared volume for the persistency of the data.
    * An the other one, contains the application. 
    This container is based on an Ubuntu image with the minimal installations needed for the compilation and the usage of the application.

The first use of the docker-compose command can take a while due to the necessity of java on the application container.